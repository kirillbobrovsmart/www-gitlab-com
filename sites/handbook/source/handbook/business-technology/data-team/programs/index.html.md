---
layout: handbook-page-toc
title: "Data Team Programs"
description: "Data Programs."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

##  Welcome to the Data Programs Page

The Data Programs page contains information about the various programs we support, ranging from onboarding to day-to-day operations.

### Data Programs

| Program Name | Type | Purpose | 
| :--- | :--- |
| [Data for Finance](/handbook/business-technology/data-team/programs/data-for-finance/) | Operational | Information to help Financial Analysts |
| [Data for Product Managers](/handbook/business-technology/data-team/programs/data-for-product-managers/) | Operational | Information to help Product Managers |
| Data for Marketing Analysts | Information to help Marketing Analysts |
| [Data for Sales Analysts](/handbook/sales/field-operations/sales-strategy/) | Information to help Sales Analysts |
| Data Onboarding | 1-Time/Ad-Hoc | Training, provisioning, and enablement of Data Engineers, Analysts, or Developers, both inside of and outside of the central Data Team. |
| Data Slack Channel | Operational | Monitoring of the [#data](https://gitlab.slack.com/messages/data/) channel to address questions for team members across the company. | 
| [Data Triage](/handbook/business-technology/data-team/how-we-work/triage/) | Operational | Daily process to ensure the data platform remains available for analytics. |

#### Data Onboarding

If you are onboarding to GitLab and will be working in the Data Program as an Engineer, Analyst, or Developer, follow these steps:

1. Open a new issue in [GitLab Data Analytics](https://gitlab.com/gitlab-data/analytics/-/issues) with the `Data Onboarding` template.
1. Give the issue a descriptive name: `Your Name - Data Onboarding`
1. Assign the issue to your Manager to add/remove relevant content


