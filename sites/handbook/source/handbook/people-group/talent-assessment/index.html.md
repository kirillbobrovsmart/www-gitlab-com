---
layout: handbook-page-toc
title: Talent Assessment 
description: "Overview of three critical pieces of GitLab's talent development program: performance, growth, and key talent."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Talent Assessment Program 
 
### Purpose

The purpose of the Talent Assessment Program is to identify and retain team members who drive the success of our organization. This is a top priority and strategic process for people managers at GitLab. Our Talent Assessment Program (which is a critical piece of our overall [Talent Development Program](/handbook/people-group/talent-development-program/)) serves as a mechanism to help mitigate a couple of our biggest company risks: [lack of performance management](/handbook/leadership/biggest-risks/#underperformance) and [losing key people](/handbook/leadership/biggest-risks/#key-people-leave). 

Additional key benefits of the Assessment Program include:

- Help facilitate career development conversations (and ensure they're occurring regularly)
- Ensure transparency and ongoing feedback between team members and their managers
- Discuss promotion readiness and plan promotions
- Succession planning
- Increase awareness of top talent across departments (during calibration sessions)


### Overview 

There are many talent assessment options, and we have opted to use a Performance/Growth Matrix (commonly known as "9-Box" in the US) and annually review Key Talent. 

GitLab's Performance/Growth Matrix is a type of talent assessment that forms part of our [Talent Development Program](https://about.gitlab.com/handbook/people-group/talent-development-program/). To ensure we remain efficient and consistent, we built a [Performance/Growth Assessment Tool](https://about.gitlab.com/handbook/people-group/engineering/assessment-tool/) to leverage during this process. 

In addition to assessing Performance/Growth, we annually review Key Talent aligned with the criteria outlined below on this page. The combination of Performance/Growth and Key Talent assessments allow us to identify team members who drive the organization’s success and use our engagement tools to retain them. 


# The Performance/Growth Matrix

| Performance ↑ <br> Growth →  | **Developing Growth** | **Growing Growth** | **Exceeding Growth** |
|-----------------|---------------|---------------|----------------|
| **Exceeding** | Team member is performing well in their current job but needs to continue to grow in current role, or has not exhibited the willingness or ability to grow in the current role.| Team member performs well in their current job, makes valuable contributions and consistently demonstrates competencies required. They have shown willingness and/or ability to further grow in their role and they may be ready to take on additional responsibilities in the next 12 months. | Team member is developing faster than the demands of their current position and/or division. Team member has been given additional assignments and has demonstrated high-level commitment/achieved significant results. Team member is ready to broaden their skill set and take on significantly greater scope and responsibility |
| **Performing** | Team member is currently meeting expectations of their role. Team member is not willing or able to absorb greater scope, impact or complexity. | Team member is currently meeting expectations and has shown willingness or ability to further grow; may not be ready to absorb greater scope, impact or complexity in the next 12 months. | Team member is contributing as expected and is meeting performance expectations. They have shown willingness and/or ability to further grow in their role and they may be ready to take on additional responsibilities in the near future. |
| **Developing** | Team member is not meeting performance expectations and there is still more to learn in the current position. There are questions about their willingness and ability to succeed in the current role long-term. | Team member has not been in the position long enough to adequately demonstrate their capacity, or may have lost pace with changes in the organization. The team member has shown the willingness and/or ability to grow in the role. | Team member is not meeting the requirements in their current role, or we have not had sufficient time to fully assess performance. This could be a result of a team member being recently promoted/new to the role. The team member has shown willingness and ability to grow in the role and could be more successful in the current role with more direction, or in another role or department that more appropriately suits their skill set. |


This matrix is an individual assessment tool that evaluates both a team members current contribution to the organization and their potential level of contribution.
It is commonly used in succession planning as a method of evaluation an organization's talent pool, identifying potential leaders, and identifying any gaps or risks.
It is regularly considered a catalyst for robust dialogue (through a calibration process) and is considered more accurate than one person's opinion.
The performance/growth matrix can be a diagnostic tool for career development.

The matrix serves as a tool to help managers assess, develop, and coach their team members - ultimately resulting in an ability to more effectively and efficiently lead teams.

## What is "Performance"?

Performance includes both results and behaviors, and is broken into three areas: Developing, Performing, and Exceeding.

### Developing

**Please note that `Developing` should not be automatically associated with underperformance. As highlighted below, `Developing` can also be used for new hires or newly promoted team members that are still ramping up in their new roles. Your manager will provide example and detail to ensure the rationale behind the `Developing` rating is communicated.**

Based on the Job Family responsibilities, [values](https://about.gitlab.com/handbook/competencies/#values-competencies) and [remote working competencies](https://about.gitlab.com/handbook/competencies/#remote-work-competencies), team members in this category are not meeting all of the expectations. There is still more to learn and improve on in the current position to achieve the desired results. This may be due to the following: 

- The team member has not been in the position/at GitLab long enough to adequately demonstrate performance.
- The team member has been recently promoted and needs time to ramp up performance in the new role. 
- The team member may have lost pace with changes in the organization. 
- The team member is not committed to the job responsibilities and/or GitLab. 
- The team member finds it difficult to perform work in a way that is aligned with our values and/or competencies. 
- The team member's performance is aligned to our values and competencies, however, they lack results for Job Family responsibilities. 
- Team member was part of a recent [transfer](https://about.gitlab.com/handbook/people-group/promotions-transfers/#department-transfers) and is still learning how to be successful on their new team.

Examples: 

- The team member needs a lot of guidance from the manager with the majority of tasks to understand requirements and deliverables expected.
- The team member is missing important due dates which is affecting the team, GitLab and/or customers. 
- The team member still needs to adapt remote working best practices. For example, the team member does not manage their own time or work to achieve results or may have difficulty with asyncronous communication. 
- The team member lacks (a part of) the knowledge, skills and abilities which are required for the role, resulting in a low quality of work being delivered.
- There is a significant mis-alignment (I.E. The team member struggles to collaborate with others, the team member does not work iteratively and key metrics are impacted, etc.) 

### Performing

Based on Job Family responsibilities, [values](https://about.gitlab.com/handbook/competencies/#values-competencies) and [remote working competencies](https://about.gitlab.com/handbook/competencies/#remote-work-competencies) team members in this category are “on track” and meeting all expectations. They can independently and competently perform all aspects of the Job Family responsibilities in a way that is aligned with our values and competencies. Their performance consistently meets the requirements, standards, or objectives of the job, and they can occasionally exceed expectations. They deliver results in a timely and accurate fashion. This performance may be expected as a result of: 

- The team member is consistent and stable in their performance.  
- The team member has the required knowledge, skills and abilities to perform in the role successfully and also applies values and competencies in their work.
- The team member has adapted to remote working and their knowledge, skills, and abilities align seamlessly to their role at GitLab. 

Examples: 
- Team member is a dependable member of the team. Their manager can trust them to achieve key metrics.
- Team member consistently works in accordance with our value and remote working competencies. 
- While consistently meeting expectations, the team member does not regularly exceed expectations.
- Team member does not frequently express interest in additional projects, responsibilities, or work outside of their immediate scope.
- The team member is exemplifying our values in their work, but might have some competencies to improve on. 

### Exceeding

Team members that are exceeding consistently surpass the demands of their current position. They demonstrate unique understanding of work beyond the assigned area of responsibility. They contribute to GitLab’s success by adding significant value well beyond Job Family requirements, [values](https://about.gitlab.com/handbook/competencies/#values-competencies), and [remote working competencies](https://about.gitlab.com/handbook/competencies/#remote-work-competencies). This performance may be expected as a result of: 

- Individual is developing more quickly than the demands of their current position. 
- They rise to the challenge when give additional assignments and achieve significant results.
- A “go-to” team member for their peers for tough problems and day-to-day guidance.
- High commitment and engagement to GitLab combined with extensive knowledge, skills, and abilities to perform in the role. 

Examples: 

- Constantly seeks opportunities to improve both self and organization. 
- The team members exemplify great ways of giving and receiving feedback - incorporating this directly in their work. 
- The team members are ambassadors for the values, take full ownership of delivering projects, and very rarely miss due dates. 

**Please note that an `Exceeding` assessment for the performance factor does not guarantee a promotion. While the performance factor is a consideration, there are several considerations that are reviewed when evaluating promotion readiness. Please work with your manager to align expectations.** 

### Expected Distribution Company-Wide

- **Developing**: 10-15%, not yet meeting all of the expectations 
- **Performing**: 60-65%, “on track” and meeting expectations 
- **Exceeding**: 25%, consistently surpasses the demands of their current role

### The Performance Factor

While the primary objective of the performance axis of the Performance/Growth Matrix is to calibrate team member performance, this axis of the matrix also directly impacts the [Performance Factor](/handbook/total-rewards/compensation/compensation-review-cycle/#performance-factor), which is a determining factor in the annual compensation review. Please reference the [Total Rewards Performance Factor page](/handbook/total-rewards/compensation/compensation-review-cycle/#performance-factor) for more detail on compensation impact.

### Measuring Performance

How can managers determine whether their team members are Developing, Performing, or Exceeding? "The Performance Factor" is determined from several components outlined below. It is _optional_ to use [The Performance Factor Workheet](https://docs.google.com/spreadsheets/d/1HHe-Vb6y6F4HXsek3sObV4IVNAGaTvXGi_9fbatT0Uo/edit#gid=241716076) for weighing the different Performance Factors.

#### Job Family Responsibilities and Functional Competencies

Performance against Job Family responsibilities, requirements, performance indicators, and functional competencies (if applicable) should be weighted at 60%. 

* Review Job Families: Look at the Responsibilities section and how your team member is performing against those responsibilities 
    * Example: [Software Engineer in Test](/job-families/engineering/software-engineer-in-test/#responsibilities)
* Review Performance indicators per Job Family: Look at the Performance indicators.
    * Example: [Software Engineer in Test](/job-families/engineering/software-engineer-in-test/#performance-indicators)
* _(if applicable)_ [Functional Competencies](/handbook/competencies/#functional-competencies)

For management roles, please also review and consider the [levels](/company/team/structure/#levels) outlined in the Organizational Structure. 

#### GitLab Competencies

Performance against these competencies should be weighted at 40%. 

* [Values Competencies](/handbook/competencies/#values-competencies)
* [Remote Working Competencies](/handbook/competencies/#remote-work-competencies)
* _(if applicable)_ [Manager and Leadership Competencies](/handbook/competencies/#manager-and-leadership-competencies) 

## What is "Growth"?

While performance is focused on the past and present, growth is focused on the future. Because of the nature of the future-focus associated with growth, it is more difficult to measure than performance, and inherently more qualitative than quantitative. A key element in determining growth is the manager and leadership observation and experience working with team members. Managers can gauge team member growth against the expectations in their current role or their growth to take on different roles across GitLab. 

Growth refers to the ability and desire of a team member to successfully assume increasingly more broad or complex responsibilities and learn new skills, as compared to peers and the roles' responsibilities outlined in their respective Job Family. This could include the growth to move up to the next level in their job family, and/or a lateral move. 

The Growth assessment helps managers determine the best growth trajectory for their team members. The growth assessment will be used as input to leverage our engagement tools, offer lateral and upwards career opportunities and do succession planning.

Growth *can change* over time as team members develop new interests, as new opportunities for growth arise, and as team members continue to broaden their knowledge, skills, and abilities. 


### Measuring Growth

There are four primary pillars to consider when measuring growth:

| "Growth" Pillar | Definition |
| --------------- | ----------------- |
| Adaptability | Demonstrating a willingness and ability to learn new skills and apply them to be successful under new, tough, or difficult conditions. |
| Expandability | Expandability outside their areas (laterally or vertically), with the willingness and ability to take on a role of greater complexity, impact, and scope |
| Consistency | Demonstrating effective problem-solving capabilities, the consistent delivery of results over time in changing circumstances, and dependability in the commitments you make. |
| Self-Awareness | The depth to which an individual recognizes skills, strengths, weaknesses, blind spots, and is able to reflect and act to improve and invest in their own development. Judgment in decision-making is also a key element of self-awareness. Judgment can be seen through our level of self-awareness in several ways, including communication, collaboration, and results. Judgment can be defined as "The ability to make considered decisions or come to well thought-out conclusions", and to do this effectively, we need to have a heightened sense of self-awareness in terms of how our communication will come across and how decisions and conclusions drawn will impact others and their willingness to collaborate. |

Below under each pillar there are a few questions that managers should consider when assessing team member growth. Please note that the answer to all of these questions does not have to be `yes` to determine a team member is "exceeding" growth, the questions are here to help guide managers through the thought and evaluation process.  

#### Adaptability

1. Does the team member adapt to change well? (I.E. realignments, management changes, company changes, etc.)
1. Does the team member seek to learn new skills to further succeed in their role?
1. Has the team member demonstrated ability to learn new things effectively under pressure?

#### Expandability

1. Does the team member demonstrate interest in other areas outside of the immediate scope of their role?
1. Does the team member invest time in learning new skills that will help them grow (laterally or vertically)?
1. Has the team member volunteered for any "stretch" projects or initiatives? 
1. Does the team member coach, mentor, and/or influence others? 

#### Consistency

1. Does the team member consistently deliver results?
1. Does the team member follow through on their commitments? 
1. Is the team member dependable? 
1. If scope or timeline change, does the team member communicate the change and provide updates to stakeholders? 

#### Self-Awareness

1. Does the team member respond to feedback aligned with our [Receiving Feedback Guidelines](/handbook/people-group/guidance-on-feedback/#receiving-feedback)?
1. Does the team member acknowledge their strengths and areas of improvement?
1. Does the team member work towards closing gaps for their improvement areas? 
1. Does the team member demonstrate sound judgment in their decision-making?

### Growth Output

After assessing team member growth based on the four pillars outlined above, managers can determine whether team member growth is currently developing, growing, or exceeding. 

#### Developing

"Developing" growth generally refers to a team member who is not working at full growth against the roles and responsibilities outlined in their Job Family. There could be a variety of reasons for this, including:

* Lack of motivation to learn new skills or take on new projects
* Motivated to do what is needed in current job, not in what is required in a higher level
* Not expressing interest or demonstrating desire to move up or laterally 

Please note that "developing" growth *does not* equate to "developing" performance, but correlates with the pillars of growth outlined in the "measuring growth" section below. 

Examples:
- A team member has recently been promoted and is struggling or hasn't had the time to expand their scope to meet the requirements of their new role.  
- A team member has actively expressed that they do not have an interest in learning new skills or take on new projects. In a day-to-day they show self-awareness but they have not shown great adaptability, expandability and consistency to further expand on their role and responsibilities. 

#### Growing

"Growing" growth generally refers to a team member who is growing in their current role and demonstrating interest in advancing (up or laterally) and they exhibit knowledge, skills, and abilities that indicate this. Team members with "growing" growth generally:

* Show interest in areas outside of their immediate scope occasionally
* Are comfortable in their current role
* Are a stable counterpart for other team members (especially for peers and more junior team members)
* Learn and apply new skills when the job calls for it; apply lessons learned to enhance success

Example: 
- A team member has demonstrated adaptability and expandability but has not been consistent in their results or self-awareness. This holds the team member back from growing to a higher level or taking on more complex responsibilities in their current role. 

#### Exceeding

"Exceeding" growth generally indicates that a team member will be ready for promotion within the next year (or when an opportunity arises). "Exceeding" growth team members:

* Frequently seek involvement in stretch project/projects outside of their scope
* Invest in their development; seek feedback to improve and applies that feedback on the job
* Demonstrate ability to learn new skills
* Actively pursue increased opportunities to influence decisions and inspire others

Example: 
- A team member has consistently and proactively looked for opportunities to fill gaps and take on stretch projects. When another team member left the team, you leaned on this team member to help keep projects moving forward until you found a suitable backfill. They regularly ask for feedback from peers and adapt their approach accordingly. This has given your team member skills beyond their current job requirements, and broader understanding of the business.

## Calibration Session Guidelines

The portion of the Performance/Growth matrix that often entails the most significant time commitment is the live calibration session of team members with leadership. The calibration session is very valuable to ensure consistency across the Job Family and level, raise any questions, and provide cross-departmental and/or cross-divisional feedback on team members to capture the assessment of different managers as opposed to the opinion of the direct manager exclusively. 

### Pre Work

It is **absolutely essential** that managers complete the required pre-work to ensure that the live calibration session is as efficient and productive as possible. Pre work includes:

- For calibration sessions you can leverage [this agenda template](https://docs.google.com/document/d/1zrlZSfdNXpSZ09uvDM71KxH42RWBJxa6U1QTCTw05iI/edit)
- Review the Job Family/Families that will be reviewed in detail
- Review our competencies 
    - [Values competencies](/handbook/competencies/#values-competencies)
    - [Remote work competencies](/handbook/competencies/#remote-work-competencies)
    - [Functional competencies](/handbook/competencies/#functional-competencies) _(if applicable)_
- Review our [Unconscious Bias](https://about.gitlab.com/company/culture/inclusion/unconscious-bias/) handbook page and consider watching our [Recognizing Bias Training](https://about.gitlab.com/company/culture/inclusion/unconscious-bias/#recognizing-bias-training) if you haven't already. 
- Review the [Performance/Growth Matrix Training Information](https://docs.google.com/presentation/d/151ys8xkOak9ifU9IPXQydZ44sb_BoMpMocWmjVonLHE/edit), which delves into definitions for each box. 
- Determine the appropriate box for each of your team members
- Add notes for each of your team members to the agenda, at least 12 hours before the session 
    - An example of notes could include:
        - 2-3 strengths/accomplishments (and supporting examples)
        - 2-3 improvement areas
        - Anything else noteworthy (I.E. recently promoted, COVID impact, etc.)
- Keep the [SBI Model](/handbook/people-group/guidance-on-feedback/#s-b-i-model) (Situation, Behavior, Impact) in mind when adding notes for strengths/accomplishments and improvement areas. Each point should include clear examples and blanket statements should be avoided. 
- Review the notes of your peers for other team members and add feedback/questions/thoughts for discussion
- If you did not attend the people manager live training session in August 2021, please watch the Performance/Growth Training video overview below:

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/siI3wRtQYQA" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

### Calibration Session

The calibration session is one of the most important pieces of the Performance/Growth Matrix process, as it provides time for managers, their peers, and their manager to calibrate. Below are a few communication guidelines to ensure efficiency and fairness during the calibration session discussion. 

#### Best Practices

* It may not be needed to discuss every team member in detail. Calibration sessions typically focuses on gaps, outliers and areas that might require additional management attention like the timeline and process for team members ready for promotion, how to improve performance for developing team members, etc.
* Be conscious of time and consider setting a (reasonable) time limit per team member being discussed. 
* Review program guidelines and avoid leniency bias.
* Refer to performance data that you may have taken on a team member throughout the past year - including 360 review feedback (if both team member and manager agree to include), performance against metrics over time, key accomplishments, etc.; this will help avoid bias like recency bias or the halo effect. Please note that when reviewing 360 data for the Q4 talent assessment, it's important to take into account areas in which the team member has improved. Two quarters is a fairly significant amount of time and team members have (hopefully) take action on improvement areas during this period, which should be reflected in the assessment. Managers and other participants in calibration discussions should be prepared with this information if asked how they determined a rating. 
* Leaders should feel comfortable highlighting team members in other functions
* Focus on discussing artifacts that are relevant to the performance factor and refrain from discussing artifacts that are not relevant to performance being developing, performing or exceeding. Examples of artifacts which are not relevant: upcoming maternity/parental leave, personal information/home situation or things that were confidentially shared and do not relate to performance. 
* Focus on facts and avoid blanket statements (I.E. [team member] has poor communication)
* **Ask questions and provide feedback!** This may seem obvious, but it is import aspect to help identify any potential bias

#### Topics To Avoid

While we want to encourage open and transparent conversation during calibration session, there are certain topics that should be avoided to ensure we respect team member privacy.

* Sharing whether team members are currently undergoing [formal performance remediation](/handbook/leadership/underperformance/#options-for-remediation). It is okay to share that team members are receiving coaching, but specifics pertaining to Written coaching, PIPs, etc. should be avoided. 
* Anything related to health (I.E. medical diagnoses, conditions, disabilities, etc.). It is okay to share that team members have a lot going on in their personal lives, but we should not go into detail. _Note that this applies for team member's friends/family as well._

## Matrix Tool

Our goal is to have the whole company use the Talent [Assessment Tool](https://about.gitlab.com/handbook/people-group/engineering/assessment-tool/) for the Performance/Growth matrix. Our People Group Engineering team has further iterated on the assessment tools' security, and all team members are now able to log in through Okta. If you have any feedback or questions regarding the tool please reach out to [People Connect](/handbook/people-group/people-connect/).

## Identifying Action 

After the calibration sessions the performance and growth outcomes can be used as input for the following: 
* [Career Development Conversations](https://about.gitlab.com/handbook/leadership/1-1/#career-development-discussion-at-the-1-1)
* Promotion planning 
* Identifing [L&D opportunities](/handbook/people-group/learning-and-development/)
* Succession Planning

# Q4 Formal Talent Assessment 

## Timeline
Below is the high level timeline for the formal assessment in FY22-Q4. Different departments may have additional due dates built into the high level timeline, so please follow up with your [People Business Partner](https://about.gitlab.com/handbook/people-group/#people-business-partner-alignment-to-division) if you have any questions. 

- **2021-11-01:** Kick off Assessment cycles for Managers
- **2021-11-08 - 2021-12-03:** Time for calibration and finalising assessments
- **2021-12-06 - 2021-12-09:** Total Rewards and E-Group review
- **2021-12-10:** Final approvals of E-Group
- **2021-12-13 - 2021-01-31:** Managers can communicate Performance/Growth Factors to team members
- **2022-01-01:** Annual Compensation Review Kicks off for Managers

### Cadence

Our e-group completes Performance/Growth Matrix multiple times a year for their direct reports. The rest of GitLab does this _at least_ once per year in Q4, with a recommended informal mid-year checkin. The formal assessment would ideally take place in November prior to the [annual compensation review](/handbook/total-rewards/compensation/compensation-review-cycle/#annual-compensation-review). 

### Eligibility

Anyone hired on or before October 31st is eligible to participate in the Q4 Performance/Growth matrix annual review. However, if the team member being assessed has been with GitLab for a period of 3 months or less, it is likely that these team members will fall into `Developing` for performance and should not expect a compensation adjustment, although they are still eligible to be reviewed.

This is because team members generally need several months to get adjusted to their role and responsibilities, and we ideally would have hired the team member at their accurate market rate. 

While there are exceptions, this is the general rule of thumb. 

### Matrix Assessment Timeline 

The Performance/Growth Matrix typically takes 4-6 weeks to complete from beginning to end. Steps are as follows:

1. Managers complete a Performance/Growth Matrix for their respective teams
1. Live session takes place for calibration with managers up to E-Group level
1. Executive Review (leadership meets to review the matrix results, key talent, promotion requests, development and performance actions, review discrepancies, and measure against previous matrix results)
1. Total Rewards performs an equality review. 
1. Final approval of E-Group member and Total Rewards. 
1. Performance and Growth results are communicated to team members. Managers can leverage [this template](https://docs.google.com/document/d/1XT1l1STUBilDdVGHXlCcmSE5ApxwVqO1Ln4Wk8e1Fpk/edit#) for communication. 
1. Annual Compensation review kicks off after Talent Assessment

## Mid-Year Check In

It is recommended (but not required) that managers have discussions on [Performance](https://about.gitlab.com/handbook/people-group/talent-assessment/#what-is-performance) and [Growth](https://about.gitlab.com/handbook/people-group/talent-assessment/#what-is-growth) with their team members early Q3 prior to the formal assessment in Q4. Benefits to having this second dedicated discussion are:

* To align 
* To provide a space for questions/clarification ahead of the formal assessment 
* To receive feedback from the team member on their own performance/growth 
* To provide feedback to the team member on their performance/growth
* To check in on progress from previous career discussions and goals
* To become more familiar and comfortable with discussing performance and growth

*Please document the mid-year discussion.* While the mid year assessment is not "formal" in the sense that it will not be the final assessment utilized to inform the annual compensation review and there will not be calibration sessions, it is important to capture what was discussed, feedback, and points addressed to help inform future discussions and assessments. This conversation can be documented in your 1:1 document, or alternatively, you may leverage [this template](https://docs.google.com/document/d/1ulyuazZpNAFnoOFDITo4fUHLDAX_bKhr242uG9x4yPE/edit#heading=h.j3xr5kqp6s24). 

#### Timeline

Mid-year conversations should be complete and documented by October 3, 2021 at the latest. Managers have a couple of options in terms of timeline and when to conduct these conversations:

1. Conduct a single conversation on Performance/Growth/and 360 feedback results aligned with the [360 feedback cycle timeline](/handbook/people-group/360-feedback/#timeline-fy22)
1. Conduct two separate conversations (one of Performance/Growth, and another on 360 feedback results after [reports come out](/handbook/people-group/360-feedback/#timeline-fy22)). If this option is chosen, 360 feedback results should be considered and reviewed to incorporate into Performance/Growth documentation after the fact to ensure we do not lose this data point. 

There will be a Manager training session on Performance and Growth delivered by PBPs on  **2021-08-02 (APAC/Americas) and 2021-08-03 (EMEA/Americas)**. "Growth" was previously referred to as "Potential". Training will be provided as we will be implementing "Growth" assessments company-wide this year.

_Note: Not all division chose to participate in the 360 feedback cycle for FY'22. This is completely fine, and we recommend that Performance/Growth conversations are conducted regardless leveraging other feedback and data points pertaining to Performance and Growth._

1. **2021-08-02 (APAC/Americas) and 2021-08-03 (EMEA/Americas)**: Manager training on Performance and Growth delivered by PBPs _(Note: "Growth" was previously referred to as "Potential". Training will be provided as we will be implementing "Growth" assessments company-wide this year.)_
1. **2021-08-04 - 2021-09-10**: Managers begin compiling any relevant data points/information to help facilitate the Performance/Growth and 360 feedback conversation with team members. Team members should also consider and reflect upon their own Performance and Growth ahead of the session.
1. **2021-09-11**: 360 reports become available for divisions that chose to participate. 
1. **2021-09-11 - 2021-10-04:** Manager and team members scheduling a synchronous discussion to discuss and document performance, growth, and 360 feedback results. 

## Communication 

In most companies the Performance/Growth Matrix is used exclusively as a management tool and results are not typically shared with team members. In the spirit of our transparency value, we want to encourage feedback with team members. **Discussion topics that arise during calibration sessions (or at any other point during the assessment process) are confidential. Please do not share with anyone other than each individual team member.**

The guidelines below are general guidelines and recommendations. However, *each department is at liberty to determine what works best for their groups in collaboration with their People Business Partner*. For all groups we recommend that managers communicate Performance and Growth factors. 

## Promotions During Assessment Cycle

The cut-off date to determine whether to assess team members at the pre-promotion level or post-promotion level for the FY'22 Q4 Talent Assessment cycle is **November 1, 2021** aligned with the cycle kick off. Guidelines are as follows: 

1. **If a team member is promoted with an effective date _before_ November 1, 2021, they should be assessed at the newly promoted level.** 
    * For example, if a Product Manager is promoted to Senior Product Manager with an effective date of October 15, 2021 in BambooHR (our SSOT for promotion effective dates), they will be assessed at the Senior Product Manager level for the Q4 Talent Assessment.  
1. **If a team member is promoted with an effective date _on or after_ November 1, 2021, they should be assessed at the pre-promotion level.**
    * For example, if a Backend Engineer is promoted to Senior Backend Engineer with an effective date of December 1, 2021, their pre-promotion assessment at the intermediate Backend Engineer level will remain.

It is important to note that because being promoted recognizes high performance both in terms of increased scope/responsibility and monetarily through compensation increase therefore a **promotion "resets" the performance factor evaluation**. 




## Best Practices for Communicating Performance/Growth Factors

After calibration sessions are done and performance and growth factors are determined, it’s time to communicate the final results with team members (after you have been notified that they have been approved by the E-Group). **Please refrain from communicating Performance factors until approval through the E-group level is communicated.** Having an extensive conversation on performance and growth with your team members is a great way to set them up for success going forward. We also want to take the opportunity to give them a perspective on their career development. Below we will take you through some of the best practices, but remember, if you are even in doubt please reach out to your manager or [People Business Partner](/handbook/people-group/#people-business-partner-alignment-to-division) for support. 

1. **Communicate performance and growth factors face-to-face over Zoom**. As a manager, this is the opportunity for you to have a conversation with your team member about their performance. Having the conversation over Zoom allows for you to have a dialogue with your team member (versus just sharing their performance and growth factor) and allows you to pick up other information, like tone and non-verbal cues which can tell you more about how someone is feeling during this conversation.

1. **Prepare for the call ahead of time.** As a manager, you should have prepared in advance of calibration discussions with your team members. Before communicating a team member’s [performance and growth factors](/handbook/total-rewards/compensation/compensation-review-cycle/#performance-factor), it’s beneficial to look back through your preparation document, notes, and anything relevant that was discussed as part of the calibration exercise. Be prepared with notes on the following: 
    * Areas the team member is doing well
    * Areas that need improvement
    * Provide specific examples for both strengths and improvement areas. The [SBI](/handbook/people-group/guidance-on-feedback/#s-b-i-model) (Situation - Behavior - Impact) model can be effective for delivering examples structured in a way that makes impact clear to team members. 
    * Think about recommendations and/or focus areas for a team member’s development

_To help facilitate the conversations we have put together a [Talent Assessment Template](https://docs.google.com/document/d/1XT1l1STUBilDdVGHXlCcmSE5ApxwVqO1Ln4Wk8e1Fpk/edit) which can be shared with the team member. The use of the template is highly recommended, but optional._

1. **Schedule a separate call or repurpose your [1:1](https://about.gitlab.com/handbook/leadership/1-1/) to discuss a team member’s performance factor**. The performance and growth factors should be communicated at the beginning of the meeting. This allows the team member time to ask questions, discuss their assessment, and most importantly, determine next steps.

1. **Protect the confidentiality of other team members** by avoiding saying things like “you were the only team member to be rated this performance factor.”

1. **Avoid making future promises** like, “In the next review, I will rate you X performance/growth factor.”

### Sample Script for Delivering a Performance Review
"Thank you for taking some time today to discuss your performance. I wanted to discuss your achievements and strengths, improvement areas, future development, and the final result of the performance assessment." 

"I assessed your performance factor as ****[Insert Performance factor]***:"
* ***Exceeding (consistently surpassing the demands of their current position)***
* ***Performing (“on track” and meeting all expectations of current position)***
* ***Developing (room to learn and grow, not currently meeting all expectations of current position)***

"I assessed your performance as ***[Insert Performance Factor]*** because:"
*  I rated your performance against your job responsibilities as ***[Insert Performance Factor]***.
    * Your strengths in your job responsibilities are [Insert strengths with supporting examples]
    * Your opportunities in your job responsibilities are [Insert opportunities with supporting examples]

* I assessed your performance against values and remote working (add management/leadership competencies if applicable) competencies as ***[Insert Performance Factor]***.
    * Your strengths in aligned competencies are [Insert strengths with supporting examples]
    * Your opportunities in aligned competencies are [Insert opportunities with supporting examples]

Your overall performance is ***[Insert Performance Factor]***. 
I'd like to thank you for your hard work, and I look forward to continuing to work together! Do you you have any questions? ***[Allow team member to ask questions and respond]*** 
As a next step, let's schedule some time to talk more about what you want to do in the future and create a career development plan together. 

### Sample Next Steps
The most important part of the performance factor discussion is to determine next steps in partnership with your team member. The team member should be the DRI for the next steps with support from you as their manager. 
Our [Career Development handbook page](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/) includes a lot of great information and resources on career development including [an individual growth plan template](https://docs.google.com/document/d/1ZjdIuK5mNpljiHnFMK4dvqfTOzV9iSJj66OtoYbniFM/edit) and [career development worksheet](https://docs.google.com/presentation/d/104AFLl-45WVHbFqQFpNL8Ad-B5_vdY39wPEEmQsEKYI/edit#slide=id.g556339813d_0_2) which should help provide structure and a plan for next steps. 
If you have any questions or concerns about next steps, please contact your manager and/or your [People Business Partner](https://about.gitlab.com/handbook/people-group/#people-business-partner-alignment-to-division). 

## FAQs 

### Performance/Growth Review General FAQs
* **How is this different from what we did in the past?** <br>
    * **Addition of the Talent Assessment Process:** In the past, we did not have a company-wide talent assessment process. Some groups leveraged a 9-box format, skill/will matrices but there was no formal program to support the review of team members. With this newly-launched process, we plan on having talent review cycles company-wide twice a year using the same assessment template. <br>
    * **Changes to the Annual Compensation Review Program:** In the past (FY20), in preparation for the Annual compensation review, managers went through a process to assess someone’s knowledge, skills, and abilities, also known as their compa group. This would directly feed as an input to the compensation review program in addition to location factor changes and cost of living adjustments. This is the second year that we replaced the compa group assessment with a performance factor. Location factor changes will continue to be incorporated into the program and we are removing cost of living adjustments. <br>

* **Why does this matter to me?** <br> 
This change will impact both team members and managers. 
    * Team Members’ performance will be assessed (developing, performing, or exceeding) as well as growth. Team Members should be prepared to have a conversation with their manager about their performance and growth in December/January. <br>
    * Managers will need to assess their team member’s performance (developing, performing or exceeding). They will also need to attend a calibration meeting in which team member’s ratings are discussed and evaluated for consistency across the team. Once the program closes, managers will be responsible for communicating performance to team members and in some organizations, growth will be communicated to team members as well. <br>

* **How does this impact the teams I lead?** 
    * As a manager, you will be required to assess your direct team member’s performance and growth. You will also be required to participate in calibration discussions in which ratings are reviewed to ensure consistency and minimize bias. After the program is approved, you will be responsible for communicating the performance axis with your team member. <br>

* **Who I can reach out to in supporting me with rolling this out with my team?**  
    * You can reach out to your manager or your [aligned People Business Partner](https://about.gitlab.com/handbook/people-group/#people-business-partner-alignment-to-division) to assist you with this process. <br>

* **How will this impact the way I work at GitLab?** 
    * This will not impact the way that you work at GitLab. Performance is measured by your job description responsibilities and competencies (remote work, values and if applicable department specific competencies). Both of these things have been in place for a while so this should not impact what you focus on or how you work. <br>

* **When should I expect these changes to occur?** 
    * We are launching the talent review process at the beginning of November. Calibration discussions will happen throughout the month. The program will be approved by early December. The Annual Compensation Review program will kick off in January. For a more detailed timeline, please review the [initial launch timeline](https://about.gitlab.com/handbook/people-group/performance-assessments-and-succession-planning/#initial-launch-timeline) above. <br>

* **As a manager, when reviewing my team members, what is the difference between Developing and Underperformance?**
    * [Underperformance](/handbook/leadership/underperformance/) is generally ongoing for a period of time with multiple attempts to remediate. "Developing" implies more of a stage of progression that is also consistent (I.E. not a sustained underperformance to meet expectations). 
    While everyone within the Developing category isn't necessarily Underperforming, a subset of team members who might be Underperforming would be grouped into the Developing category along with those team members who are not underperforming but are simply new to the role. 
    Team members who are on a formal Underperformance remediation plan, such as a [Performance Improvement Plan](/handbook/leadership/underperformance/#performance-improvement-plan-pip), will not be eligible for a compensation increase as a result of the [Annual Compensation Review](/handbook/total-rewards/compensation/compensation-review-cycle/#performance-factor).

* **How should I factor in COVID impact when assessing performance and growth?** 

    When assessing team members who have had COVID impact that has affected their work, (reduced schedules, modified working hours, more extended time off, mental health impact, etc.) please keep in mind:

    -  Team members should be assessed at the _agreed upon and expected level_. For example, if manager and team member have agreed that the team member will be working at 80% for a period of time, the team member's performance should be assessed at 80% during that period (not at 100%).  
    - We should be looking _holistically_ at performance and growth. This performance review should assess the team member's last 12 months of tenure (or less if the team member has joined more recently). Please ensure you are mindful of recency bias, particularly for those team members who have struggled with COVID impact. 

    Team members - It is very important that you are transparent with your manager if you are experiencing significant impact from COVID that has impacted your overall performance. Managers cannot be mindful of the points above during assessment periods if they are not aware and expectations are not aligned. If it is helpful, feel free to leverage the [COVID Self-Evaluation](/handbook/total-rewards/benefits/covid-19/#self-evaluation) template to facilitate a discussion with your manager.

* **Will my Growth assessment impact my compensation?** 

    No. Presently, only the performance assessment ties directly to compensation.

    If you have any questions, please reach out to your aligned [People Business Partner](/handbook/people-group/#people-business-partner-alignment-to-division). 

#### Performance/Growth Communication FAQs:
* **Can my performance factor be changed?**
    * At this point, the performance factor cannot be changed. You should work with your manager to develop a plan to develop your performance for future assessments. We plan to evaluate performance factors every 6 months.
* **Does this mean I am getting an X% increase in the upcoming Annual Salary Review?**
The performance factor will be one of the factors that will be considered in for the [Annual Compensation review](/handbook/total-rewards/compensation/compensation-review-cycle/#annual-compensation-review). The recommended increases displayed in the handbook are there to ensure company wide consistency. The factors that will be taken into account for the Annual Compensation review are: 
    *   Your hire date (new hires hired after October 31st are ineligible for compensation review)
    *   Other compensation increases received over the last year
    *   Timing of promotion and/or role changes
    *   Your current placement within the compensation band
    *   Location factor 
    *   Role benchmark updates 
    *   Company budget for compensation increases
* **When is the next opportunity for my performance to be reviewed?**
    * We plan to conduct the talent review process twice per year. One mid-year (Q2) and one at the end of fiscal year (Q4). 
* **What can I do to change my performance factor in the future?**
    * There are a lot of things that you can do to change your performance factor in the future. Please work with your manager on a career development plan and discuss actions that can be taken to improve. 
* **Can my Growth rating be changed?**
    * Your growth factor cannot be changed. Your manager assessed your growth factor and this would be an opportunity for you and your manager to align on your growth opportunities. This discussion can also be an input into your career development discussion. 
* **What can I do to change my Growth rating in the future?**
    * There are many things that a team member might do to change their growth factor in the future. Talk with your manager about how you might be able to develop a plan to increase your [expandabitlity, adaptability, consistency and self-awareness](/handbook/people-group/talent-assessment/#adaptability). Do you currently possess all the skills needed for your current role? Have you identified what skills you’d like to learn and develop in? Have you determined what your career goals are and are you actively working towards those goals? Are there additional projects that you might be able to take on to stretch your current responsibilities and/or skills? These are some discussion questions that might help you and your manager align on a future plan for growth in potential. 
* **My team member is currently on a performance remediation plan; how should I handle?**
    *  You should continue to move forward with communicating their performance factor to them and use the meeting as an opportunity to reinforce the expectations of meeting the goals highlighted in the plan.
* **My team member has recently been promoted - but we assessed the Performance/Growth prior to the promotion. How do I communicate that to the team member?**
    *  We recognise a timing of a promotion can sometimes coincide with the talent assessment. It's important to discuss with the team member whether the talent assessment was based on the previous or new role. Being promoted recognizes high performance both in terms of increased scope/responsibility and monetarily through compensation increase and therefore a **promotion "resets" the performance/growth factor evaluation** if assessed prior to the promotion. With the promotion the job responsibilities and scope can change significantly, therefore a new assessment should be made at the next talent assessment period. We would recommend to review the job responsibilities of the new role and identify focus points for growth in the new role. 
 

## Succession Planning

The succession planning process starts with leaders doing a [performance/growth matrix](/handbook/people-group/performance-assessments-and-succession-planning/#the-performancepotential-matrix) of each of their direct reports.

The resulting charts are reviewed with peers, e.g. all other executives, all other senior leaders in the function, etc.

| Person    | Jane Doe | John Doe |
|-----------|---|---|
| Role      | Job Title  | Job Title  |
| Emergency (ER) | Someone who could take over this role if the current person were affected by a [lottery factor](/handbook/total-rewards/compensation/#competitive-rate) or had to take emergency leave |   |
| Ready Now (RN) | Someone who could be promoted into the role today  |   |
| Ready in less than 1 year (R1) | Someone who could be trained and elevated into the role in less than 1 year  |   |
| Ready in 1 year or more (R1+) | Someone who could be trained and elevated into the role in 1 year or more  |   |

## Resources

| Resource | Purpose |
| ------ | ------ |
| [Performance/Growth Assessment tool](https://about.gitlab.com/handbook/people-group/engineering/assessment-tool/). | This tool allows managers to place their team members in the Performance/Growth Matrix and helps facilitate discussion during calibration sessions. |
| [Values competencies](/handbook/competencies/#values-competencies) | Values competenices form an important part of the Performance Factor evaluation and are important to review prior to beginning the assessment period. |
|[Remote work competencies](/handbook/competencies/#remote-work-competencies) | Remote work competenices form an important part of the Performance Factor evaluation and are important to review prior to beginning the assessment period. |
| [Functional competencies](/handbook/competencies/#functional-competencies) | Functional competenices (for groups that have them developmed) can also influence the Performance Factor and should be reviewed prior to the assessment period. |
| [Unconscious Bias](https://about.gitlab.com/company/culture/inclusion/unconscious-bias/) handbook page and [Recognizing Bias Training](https://about.gitlab.com/company/culture/inclusion/unconscious-bias/#recognizing-bias-training) | It is important to me mindful of unconscious bias always, and especially during talent reviews and assessments. It is highly recommended that you review the handook page and watch the training. |
| [Performance Factor](/handbook/total-rewards/compensation/compensation-review-cycle/#performance-factor) handbook page | This page is the SSOT to review the Performance Factor's impact on compensation. |

# Key Talent

## Key Talent Criteria  

Key Talent makes up roughly ~10% of the population. Key Talent represents team members who have a significant impact on GitLab's success. They deliver quality results which are instrumental in moving critical company initiatives forward, and do so consistently in alignment with our values. These team members are often recognized as experts in their current role. Team members, at any level, can be considered Key Talent if they meet at least one of the following criteria:

If the team member were to leave there would be:

- Critical disruption to the product
- Critical disruption to ARR ([Annual Recurring Revenue](/handbook/sales/sales-term-glossary/arr-in-practice/)) - including delays for customers
- Substantially impacts company ability to achieve significant milestones
- Substantial impact to a significant process
- Significant impact to the operation of the functional area

## Key Talent and Performance

Performance and being identified as key talent can influence one another, but they are _not directly related_. For example:

- A team member that has a performance assessment of [“exceeding”](/handbook/people-group/performance-assessments-and-succession-planning/#exceeding), is not automatically indicated as key talent. Perhaps they are doing exceptionally well in their role, but the impact to the business would not be considered “critical” if they were to leave.
- A team member that is new to the role and still ["developing"](/handbook/people-group/performance-assessments-and-succession-planning/#developing) could be identified as key talent if they work in an area of the business that is highly specialized and particularly difficult to hire for. 

_Note: Intentionally hoarding knowledge is in direct conflict with our [transparency value](/handbook/values/#transparency) and is viewed as a performance issue as we measure performance based on alignment with our values._

## Process To Determine Key Talent

The first time we assessed Key Talent in March FY'22, Key Talent was determined at the ["all-directs" level](https://about.gitlab.com/company/team/structure/#all-directs). The All-Directs group is made up of anyone who reports directly to the e-group. It is made up of some ICs, some managers, some directors, and some senior leaders. 

It is important to have a holistic view of all team members when determining who meets the key talent criteria, which is why we require a certain scope when assessing key talent in the organization. There are several factors that can help determine the level at which key talent should be assessed, including things like reporting lines and span of control. As a general rule, if a people manager meets the following requirements, key talent recommendations should start at their level:
- Senior Manager level or above
- 20+ direct and indirect reports

The process to determine Key Talent is as follows: 

   1. Senior Managers (if applicable), Directors, and Senior Directors can propose Key Talent to their manager and ultimately [All Directs](https://about.gitlab.com/company/team/structure/#all-directs) leader by highlighting team members in their division's key talent sheet. The fields that will be captured mirror [this template for the sheet](https://docs.google.com/spreadsheets/d/1SoLomy3c-1RD4Dpyc35PV5lYt34cG07vaQAim96JT1k/edit#gid=0) explaining how team members meet the Key Talent [criteria](/handbook/people-group/talent-assessment/#key-talent). 
   2. The Senior Manager (if applicable), Director, Senior Directors, VP, and People Business Partner (PBP) discuss and calibrate the Key Talent nominations.
   3. All proposals are gathered in one overview sheet for the Department. 
   4. The PBP can use this sheet to review distributions levels across diversity factors and department/team/division.
   5. A final proposal goes to the E-Group member who adds in their Key Talent and reviews for final approval on Division level.
   6. PBP and E-Group member deliver sheet to Total Rewards to be uploaded in BHR.

## Organizational Value

A very small portion of our business (roughly ~10% of the population) is considered to be key talent. As such, a team members’ value in the organization should not be determined based on whether or not they are identified as key talent. 

While some team members are identified as key talent, this does not mean the rest of our team is not valued and important to our organization’s success. Aligned with our [expected performance distribution](/handbook/people-group/performance-assessments-and-succession-planning/#expected-distribution-company-wide), approximately 60-65% of our team are core performers (or ["performing"](/handbook/people-group/performance-assessments-and-succession-planning/#performing)). Core performers are responsible for keeping things consistently moving forward. This group comprises the largest population in companies across the board for a reason and is an essential part of any organization’s success. 

## Impact of Being Identified as Key Talent 

We want to make sure we use engagement tools to retain Key Talent. A few of our primary engagement tools are: [Learning & Development](/handbook/people-group/learning-and-development/), growth opportunities and compensation.

* **Learning & Development:** We want to support the growth of Key Talent and their competencies, skills, and knowledge by providing them with the tools they need and the opportunities to progress their own personal and professional development goals. As a people manager it’s your priority to identify [L&D opportunities](/handbook/people-group/learning-and-development/) for your team. 
* **Growth Opportunities:** As a people manager, it’s important to have career conversations and identify growth opportunities with your team members. For Key Talent, it’s especially important to discuss their motivation and ambitions. Growth opportunities can be both horizontal and vertical. Examples of growth opportunities are: expanding the scope within a team member's current role, a lateral move to a new role, or a promotion to the next level.  
* **Compensation:** At GitLab, compensation = cash + equity + benefits. We have a [market based approach](/handbook/total-rewards/compensation/#market-based-approach) and differentiate pay based on performance. Our Annual Compensation Review Cycle and Annual Equity Refresh program allow us to differentiate compensation based on performance and for Key Talent. 
* **Succession Planning:** As team members identified as Key Talent are critical to roles that have a high impact on the company's success, it is important that we identify successors to avoid any single points of failure within the organization.

**Note that being considered or designated as a key talent one year, does not mean or guarantee that a team member will be considered or designated as a key talent moving forward.**
