import LogRocket from 'logrocket';

(function () {
    // Only initialize LogRocket in production - we technically run Middleman and Webpack in "production" mode for review apps,
    // so this will check if the script is running on a site with the `about.gitlab.com` domain. That should skip review apps appropriately.
    if (window.location.hostname.indexOf('about.gitlab.com') !== -1) {
        initializeLogRocket();
    }
})()

function initializeLogRocket() {
    LogRocket.init('9jygng/gitlab', {
        shouldDebugLog: false,
        dom: {
            inputSanitizer: true,
        }
    });

    window.addEventListener('DOMContentLoaded', function() {
        registerCustomEventOnLoginLinks();
    })
}

// We use this to stop LogRocket from retaining recordings of users who just visit to log in.
function registerCustomEventOnLoginLinks() {
    const loginLinks = document.querySelectorAll('[data-logrocket-id="login"]');
    loginLinks.forEach(link => {
        link.addEventListener('click', (event) => {
            event.preventDefault();
            LogRocket.track('Clicked on login link');
            location.assign(event.target.href);
        });
    });
}
