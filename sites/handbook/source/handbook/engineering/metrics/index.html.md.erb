---
layout: handbook-page-toc
title: "Engineering Metrics"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Centralized Engineering Metrics

Our centralized engineering dashboards provide a set of common metrics that capture the overall health of the entire R&D Product/Engineering structure, with drill downs into every stage and group.

This work is the product of the team working in our unified [engineering metrics task process](/handbook/engineering/quality#engineering-metrics-task-process).
The inception of this initiative can be see in this [epic](https://gitlab.com/groups/gitlab-org/-/epics/3580).

### Engineering Metrics Dashboards

The links below take you to a handbook dashboard page which covers metrics from the Development, Infrastructure, Quality, UX, and Security Departments.

* [Dev Section Dashboards](/handbook/engineering/metrics/dev/)
  * [Create Stage Dashboards](/handbook/engineering/metrics/dev/create)
  * [Plan Stage Dashboards](/handbook/engineering/metrics/dev/plan)
  * [Manage Stage Dashboards](/handbook/engineering/metrics/dev/manage)
  * [Ecosystem Stage Dashboards](/handbook/engineering/metrics/dev/ecosystem)
* [Ops Section Dashboards](/handbook/engineering/metrics/ops/)
  * [Verify Stage Dashboards](/handbook/engineering/metrics/ops/verify)
  * [Package Stage Dashboards](/handbook/engineering/metrics/ops/package)
  * [Configure Stage Dashboards](/handbook/engineering/metrics/ops/configure)
  * [Monitor Stage Dashboards](/handbook/engineering/metrics/ops/monitor)
  * [Release Stage Dashboards](/handbook/engineering/metrics/ops/release)
* [Sec Section Dashboards](/handbook/engineering/metrics/sec/)
  * [Secure Stage Dashboards](/handbook/engineering/metrics/sec/secure/)
  * [Protect Stage Dashboards](/handbook/engineering/metrics/sec/protect/)
* [Growth Section Dashboards](/handbook/engineering/metrics/growth)
* [Fulfillment Section Dashboards](/handbook/engineering/metrics/fulfillment/)
* [Enablement Section Dashboards](/handbook/engineering/metrics/enablement/)

These handbook dashboard pages are populated from 5 filterable Sisense dashboards.
* [Development Embedded Dashboard](https://app.periscopedata.com/app/gitlab/681347/Development-Embedded-Dashboard)
* [Quality Embedded Dashboard](https://app.periscopedata.com/app/gitlab/736012/Quality-Embedded-Dashboard)
* [Infrastructure Embedded Dashboard](https://app.periscopedata.com/app/gitlab/798401/Infrastructure-Embedded-Dashboard)
* [UX Embedded Dashboard](https://app.periscopedata.com/app/gitlab/736036/UX-Embedded-Dashboard)
* [Security Embedded Dashboard](https://app.periscopedata.com/app/gitlab/758795/Appsec-Embedded-Dashboard)

#### How to navigate Engineering Metrics Dashboards
For a section with multiple stages or a stage with multiple groups, these metrics are repeated further down the page to give team members the ability to view these metrics at a section, stage, or group level. To navigate to section metrics, click on the `___ Section Dashboards` link above. To navigate to stage metrics, click on the `___ Stage Dashboard` link above or from the header link in the section dashboards pages. To navigate to the group metrics, find the stage that contains the group and click on the corresponding `___ Stage Dashboards` link above.

Groups are extracted from labels applied on an MR or issue. For example, the `~group::access` label is used to query MRs or issues attached to the Access group. If a group is missing from the filter dropdown in the embedded dashboards above, check if it’s included in this [spreadsheet](https://docs.google.com/spreadsheets/d/1WMS7bUHRoFBEjtP6dVbMn438NoeA-PECBECZNnSbu8E/edit#gid=0). If it’s missing, add a new row with the corresponding stage and section. This spreadsheet flows to our data warehouse once a day so you may not see updates in Sisense until the next day. In order to display this new group in the handbook dashboard pages, please reach out to a member of the [Engineering Analytics Team](https://about.gitlab.com/handbook/engineering/quality/engineering-analytics-team/#how-to-engage-with-us) to get this added to the handbook dashboard page filters.

For the [Development Embedded Dashboard](https://app.periscopedata.com/app/gitlab/681347/Development-Embedded-Dashboard), metrics are filtered based on what group each team member comes from. Group comes from the `job title specialty` field in Bamboo HR. Per the [handbook](https://about.gitlab.com/handbook/people-group/promotions-transfers/#job-information-change-in-bamboohr), managers are responsible for updating the `job title speciality` field in for any/all specialty changes. If a team member isn’t counted in the organization metrics but their group information is correct in Bamboo HR, check that the [notification email in their GitLab.com profile](https://gitlab.com/-/profile/notifications) matches the work email in Bamboo HR. If there is a mismatch, update the Bamboo HR profile email to match the [GitLab.com notification email](https://gitlab.com/-/profile/notifications). If a group was recently added or moved to a different sub department, update this [mapping file](https://docs.google.com/spreadsheets/d/1WMS7bUHRoFBEjtP6dVbMn438NoeA-PECBECZNnSbu8E/edit#gid=0). This mapping file links each job title speciality to the corresponding stage and section and flows to our database once a day.

### Metrics list

The Engineering Metrics listed here are available for all product group teams. The indicators captured here may or may not roll into an existing KPI/PI.
The Engineering Analytics team reserves the urgency for these dashboards to provide timely visibility without the requirement of having all indicators be a KPI/PI at the department level.

#### Development indicators

* MRs vs Issues
* MR Rate, this rolls up to a KPI for the Development department
* Open MR Review Time (OMRT), this rolls up to a KPI for the Development department
* MRs by team members vs Community
* Merged Product MRs by Type
* Feature flags older than 2 months

#### Infrastructure indicators

* S1 Open InfraDev Age
* S2 Open InfraDev Age
* InfraDev past SLO
* Corrective Actions past SLO
* Open S1/S2 InfraDev Issues

#### Quality indicators

* S1 Open Bug Age (OBA)
* S2 Open Bug Age (OBA)

#### UX indicators

* UX Debt open/close
* Average days to close UX Debt
* Issues with Actionalble Insights

#### Security indicators

* Average Age of currently open bug vulnerabilities, this rolls up to a KPI for the Security department

### Helpful pointers

* Review the chart regularly and take notes of your group, stage or section's trends.
* Take note of anything that might be impacting the team's capacity such as holidays or increased PTO.
* Take note of your team's focus on community contribution as an example. If the team is able to consistently merge MRs in this categories, celebrate it.
* If you see a large amount `undefined`, spend some time to review your team's issues and MRs and add labels so we can get a more accurate classification.

## Merge Request Rate

Merge Request (MR) Rate is a measure of productivity and efficiency. The numerator is a collection of merge requests to a set of projects. The denominator is a collection of people. Both are tracked over time (usually monthly).

MR Rate begins with an `[Identity]` prefix which defines the group of people (the denominator) taken into calculation. This is usually a Division, Department, Sub-Department, or Team name from our [Organizational Structure](/company/team/structure/#organizational-structure). The calculation for MR rate is the number of authored MRs by the team members divided by the number of team members in the group. For example:
* [Engineering MR Rate](/handbook/engineering/performance-indicators/#engineering-mr-rate)
* [Development Department MR Rate](/handbook/engineering/development/performance-indicators/#development-department-mr-rate)

Previously MR Rate was called "Narrow MR Rate," but [that term was removed](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/78269).

### Examples

Team A consists of 5 members. In the past month, there were 200 merged MRs:
* 100 MRs were authored by Team A team members
* 50 MRs were authored by people from  GitLab employees not in Team A.
* 50 MRs were authored by people from the wider-community.

Team A's MR Rate for that month would be: (100 / 5) = 20

## Work Type Classification

We use the following type labels to classify our Issues and Merge Requests.

1. `~"type::bug"`: Defects in shipped code. Read more about [features vs bugs](/handbook/product/product-processes/#issues).
1. `~"type::feature"`: Any MR that contains work to support the implementation of a feature and/or results in an improvement in the user experience. Read more about [features vs bugs](/handbook/product/product-processes/#issues).
  - `~"feature::addition"`: Refers to the first MVC that gives GitLab users a foundation of new capabilities that were previously unavailable. For example, these issues together helped create the first MVC for our Reviewer feature: [Create a Reviewers sidebar widget](https://gitlab.com/gitlab-org/gitlab/-/issues/237921), [Show which reviewers have commented on an MR](https://gitlab.com/gitlab-org/gitlab/-/issues/10294), [Add reviewers to MR form](https://gitlab.com/gitlab-org/gitlab/-/issues/216054), [Increase MR counter on navbar when user is designated as reviewer](https://gitlab.com/gitlab-org/gitlab/-/issues/255102)
  - `~"feature::enhancement"`: Refers to GitLab user-facing improvements that refine the initial MVC to make it more useful and usable. For example, these issues enhance the existing Reviewer feature: [Show MRs where user is designated as a Reviewer on the MR list page](https://gitlab.com/gitlab-org/gitlab/-/issues/237922), [Display which approval rules match a given reviewer](https://gitlab.com/gitlab-org/gitlab/-/issues/233736), [Add Reviewers quick action](https://gitlab.com/gitlab-org/gitlab/-/issues/241244)
1. `~"type::maintenance"`: Refers to refinements to an existing feature that are not GitLab user-facing and not related to `~"type::bug"` resolution. This could include `~"technical debt"` and industry-standard updates such as work towards Rails upgrade. For example: [Updating software versions in our tech stack](https://gitlab.com/gitlab-org/ci-cd/codequality/-/issues/22), [Recalculating UUIDs for vulnerabilities using UUIDv5](https://gitlab.com/gitlab-org/gitlab/-/issues/212322)
1. `~"type::tooling"`: MRs related to engineering tooling.
  - `~"tooling::pipelines"`: MRs related to pipelines configuration.
  - `~"tooling::workflow"`: MRs related to improvements of the engineering workflow and release tooling like Danger, RuboCop, linters, etc.

If these labels are missing, it will be tracked in the `undefined` bucket instead.
The Engineering Manager for each team is ultimately responsible for ensuring that these labels are set correctly.

### Legacy types

#### `~"backstage"` go-forward guidance

`~"backstage"` was intended to be changes that were done to keep product development running smoothly. Over time, `~"backstage"` was also being used for pre-feature work and has become unclear and confusing. `~"backstage"` was deprecated as part of <https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/488>. This will be removed with <https://gitlab.com/gitlab-org/quality/triage-ops/-/issues/483>.

This guidance may be helpful if you are wondering the go-forward type label based on your use case for applying `~"backstage"`:

- `~"type::maintenance"`
  - for industry standard and refactoring changes such as: `~"technical debt"`, `~"railsx.y"`, `~"Architecture Decision"`, non-`~"security"` `~"dependency update"`
  - for addition or updates to specs for existing GitLab features
- `~"type::feature"`
  - and `~"feature::addition"` for all changes related to the release of a new feature
  - and `~"feature::enhancement"` for user-facing improvements that refine the initial MVC to make it more useful and usable.
- `~"tooling::workflow"` for changes to engineering workflows such as `~"Danger bot"`, `~"static analysis"`, release tooling, Docs tooling changes
- `~"tooling::pipelines"` for changes to project pipeline configurations

#### `~"Community contribution"` go-forward guidance

`~"Community contribution"` was intended to track community contributions as a top-level type, but it's now only a facet label and a merge request should always get a proper type label set in addition.

Community contributions are welcome in all areas of GitLab, so any type label can be set on `~"Community contribution"` merge requests.

#### `~"security"` go-forward guidance

`~"security"` was intended to track security-related merge requests as a top-level type, but it's now only a facet label and a merge request should always get a proper type label set in addition.

This guidance may be helpful if you are wondering the go-forward type label based on your use case for applying `~"security"`:

- `~"type::feature"` for new security features that aren't fixing an existing vulnerability
- `~"type::bug"` for any other security changes

#### `~"documentation"` go-forward guidance

`~"documentation"` was intended to track documentation-only merge requests as a top-level type, but it's now only a facet label and a merge request should always get a proper type label set in addition.

This guidance may be helpful if you are wondering the go-forward type label based on your use case for applying `~"documentation"`:

- `~"type::feature"` for new feature documentation (this type would usually be already set on merge requests that introduce a new feature)
- `~"type::maintenance"` for any other documentation changes

### Stage and Group labels

In the spirit of "Everyone can Contribute" it's natural that members in a group will contribute to another group.

We allow flexibility where the parent `devops::xxx` and child `group::xxx` label may not match. For example:
* In the case where labelling was corrected by a human.
* When working on shared `frontend`, `backend` components or `type::tooling` work that spans multiple groups.

If a contribution happens across groups, we leave it to the discretion of the engineering and product manager to change the `group::xxx` label to reflect which group worked on it.
They can also decide if they want to move over the `devops::xxx` as well or keep it to reflect the product area.
The [triage bot](https://gitlab.com/gitlab-org/quality/triage-ops/) automatic labelling we will not override existing labels.

## Projects that are part of the product

In the MR Rate and Volume of MR calculations, we consider MRs from projects that contributes to the overall product efforts.

The current list of projects are identified in the [`gitlab-data/analytics`](https://gitlab.com/gitlab-data/analytics) project for the following system databases:

| System Database | File |
|-----------------|------|
| GitLab.com      | [`projects_part_of_product.csv`](https://gitlab.com/gitlab-data/analytics/blob/master/transform%2Fsnowflake-dbt%2Fdata%2Fprojects_part_of_product.csv) |
| ops.gitlab.net  | [`projects_part_of_product_ops.csv`](https://gitlab.com/gitlab-data/analytics/blob/master/transform%2Fsnowflake-dbt%2Fdata%2Fprojects_part_of_product_ops.csv)


### Updating the list of projects

The guidelines for inclusion in the `is_part_of_product` lists are:

- Included with the product as apart of a GitLab Omnibus or Cloud Native installation
- Support product development efforts
- Support the delivery and release process to GitLab SaaS

Follow these steps to request a new project to be tracked:

1. Create a merge request to the GitLab.com or ops.gitlab.net project list from above.
1. Assign the merge request to the [Manager of Engineering Productivity team](https://gitlab.com/kwiebers)
1. The Manager of the Engineering Productivity team will work with the [Engineering Analyst](https://gitlab.com/lmai1) to determine the changes to MR Rate metrics and provide validation for the projects
1. The [VP of Development](https://gitlab.com/clefelhocz1) is the DRI to approve the list of projects. Upon approval the merge request author should ask in #data for assistance by a member of the Data Engineering team to merge.

There is no need to remove archived projects from the `is_part_of_product` list. Removal of projects will remove historical merge requests from metrics and reduce Merge Request rates.

Please reach out to a member of the [Engineering Productivity team](/handbook/engineering/quality/engineering-productivity/) if more assistance is needed


## Guidelines

*  Each KPI chart is a timeseries chart.
    - The `URL` property is only used to link to a chart until it is an embedded Sisense chart.
    - Use HTML hyperlinks `<a>` in description text if we need to link out to a supporting artifact e.g. Epics or Issues.
    - Use Purple bars to denote values.
    - Use a Red stepped-line for timeseries target.
    - Directional targets will be used:
      - `Above ...`
      - `Below ...`
      - `At ...`
      - `At or above ...`
      - `At or below ...`
    - Optional: Use a Black line for rolling average.
    - Optional: Use a Gray line for supporting indicator in the background.
*  For bar charts, the current month should be Green and subsequent months Purple. Highlighting the current month in a different color helps to indicate that data for the current month is not complete.
    - This can be quickly implemented via a `case` `when` clause in Sisense. Example below:
    - `CASE WHEN date_month < date_trunc('month',current_date) THEN MEDIAN(open_age_in_days) ELSE NULL END AS "Historical Median Open Days",`
    - `CASE WHEN date_month = date_trunc('month',current_date) THEN MEDIAN(open_age_in_days) ELSE NULL END AS "Current Median Open Days",`
* List a DRI for the KPI/PI if the metric is being delegated by the VP of that Engineering department.
*  Each Sisense dashboard for KPIs should consider the following settings to ensure timely updates:
    - [Setting up auto-refresh](https://about.gitlab.com/handbook/business-ops/data-team/platform/periscope/#sts=Requesting%20Automatic%20Dashboard%20Refresh) for a frequency that fits the KPI
    - [Excluding Dashboards from Auto Archive](https://dtdocs.sisense.com/article/auto-archive)
*  Each KPI should have a standalone dashboard with a single chart representing the KPI and a text box with a link back to the handbook definition.
    - In Sisense, [create a shared dashboard link](https://dtdocs.sisense.com/article/share-dashboards) to get the shared dashboard ID.
    - In Sisense, [use the Share Link action of the chart](https://dtdocs.sisense.com/article/chart-options#ShareLink) to get the chart (widget_id) and the dashboard ID.
    - Add the `shared_dashboard`, `chart` , and the `dashboard` key-value pairs to the [corresponding Performance Indicators data file](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/performance_indicators/) under the `sisense_data` property
*  Multi-series performance indicators should consider the following guidelines:
    * If series are mutually exclusive, use stacked bars for each series with a monthly time series
    * If series are not mutually exclusive, use grouped bars for each series with a monthly time series
    * Do not graph any targets in the chart.
    * Current month styling guidelines will not apply
*  Avoid `:` in strings as it's an important character in YAML and will confuse the data parsing process. Put the string in "quotes" if you really need to use a `:`
